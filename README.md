# Web Validators

A collection of command line tools for validating HTML and CSS websites.

- [Install](#install)
- [Uninstall](#uninstall)
- [Update](#update)
- [Usage](#usage)
  - [CSS Validator](#css-validator)
  - [HTML Validator](#html-validator)
- [Explanation](#explanation)
- [Customization](#customization)
- [Credits](#credits)

## Install

1. Install Python `3.1` or higher.
2. Install [Selenium][1] module for Python
3. Clone this repository:
`git clone https://gitlab.com/caton101/web-validators.git`
4. Enter project directory:
`cd web-validators`
5. Run install script:
`sudo make install`
6. Delete the project directory:
`cd .. && rm -r web-validators`

**Notes**:

- Minimum version was detected with [Vermin][0] and may not be accurate. My testing environment is Python version `3.9`.
- `sudo` is not required, but it does need superuser privileges. Tools like [doas][2] are fine.

**Magic Script**:

```sh
git clone https://gitlab.com/caton101/web-validators.git
cd web-validators
sudo make install
cd ..
rm -rf web-validators
```

## Uninstall

1. Clone this repository:
`git clone https://gitlab.com/caton101/web-validators.git`
2. Enter project directory:
`cd web-validators`
3. Run uninstall script:
`sudo make uninstall`
4. Delete the project directory (optional):
`cd .. && rm -r web-validators`
5. Delete the [dependencies](#Install) (optional)

**Notes**:

- If you have a local repository of this project, it is preferred *not* to clone a second time. Navigate to the local repository and follow steps 3 onward.
- `sudo` is not required, but it does need superuser privileges. Tools like [doas][2] are fine.

**Magic Script**:

```sh
git clone https://gitlab.com/caton101/web-validators.git
cd web-validators
sudo make uninstall
cd ..
rm -rf web-validators
```

## Update

1. Clone this repository:
`git clone https://gitlab.com/caton101/web-validators.git`
2. Enter project directory:
`cd web-validators`
3. Run reinstall script:
`sudo make reinstall`
4. Delete the project directory:
`cd .. && rm -r web-validators`

**Notes**:

- If you have a local repository of this project, it is preferred *not* to clone a second time. Navigate to the local repository, use `git pull` and follow steps 3 onward.
- `sudo` is not required, but it does need superuser privileges. Tools like [doas][2] are fine.

**Magic Script**:

```sh
git clone https://gitlab.com/caton101/web-validators.git
cd web-validators
sudo make reinstall
cd ..
rm -rf web-validators
```

## Usage

These validation tools work like any other standard UNIX-like utility. All arguments passed to these tools are assumed to be file paths.

**NOTE**: Running any validator will make a `geckodriver.log` file in the current directory.

### CSS Validator

- Validate a single CSS file:
`css-validator mypage.css`
- Validate multiple CSS files:
`css-validator *.css`
- Validate multiple CSS files from another directory:
`css-validator /path/to/css/*.css`

Example output for `css-validator test_cases/*.css`:

```txt
Checking filepath(s): test_cases/bad.css test_cases/good.css
Results for test_cases/bad.css:
==========
Sorry! We found the following errors (1)
URI : TextArea
43 Parse Error [go]

Results for test_cases/good.css:
==========
Congratulations! No Error Found.
```

### HTML Validator

- Validate a single HTML file:
`html-validator mypage.html`
- Validate multiple HTML files:
`html-validator *.html`
- Validate multiple HTML files from another directory:
`html-validator /path/to/html/*.html`

Example output for `html-validator test_cases/*.html`:
```txt
Checking filepath(s): test_cases/bad.html test_cases/good.html
Results for test_cases/bad.html:
==========
Error: End of file seen and there were open elements.
At line 76, column 51
 Message">↩

 Error: Unclosed element form.
 From line 10, column 5; to line 10, column 10
 </h1>↩    <form>↩

 Warning: Consider adding a lang attribute to the html start tag to declare the language of this document.
 From line 1, column 16; to line 3, column 6
 type html>↩↩<head>↩    <
 For further guidance, consult Declaring the overall language of a page and Choosing language tags.
 If the HTML checker has misidentified the language of this document, please file an issue report or send e-mail to report the problem.

 Results for test_cases/good.html:
 ==========
 Document checking completed. No errors or warnings to show.
```

## Explanation

These tools have nothing complex inside the source code. It uses a headless Firefox instance to upload files to the appropriate validator ([CSS][3] or [HTML][4]).

Both program work as follows:

1. Gather a list of files using the `argparse` module (mostly to provide built-in usage information)
2. For each file:
    1. Save file contents to a buffer
    2. Spawn a headless Firefox instance
    3. Paste file contents into the validator's text area
    4. Submit the webpage form
    5. Scrape the results page for warnings or errors
    6. Close the headless Firefox instance

## Customization

These tools offer customization in the form of global variables at the top of the file (under the `# SETTINGS` comment). Use this table for more information.

| Variable          | Type | Description                                                       |
| :---------------- | :--- | :---------------------------------------------------------------- |
| DEBUG             | bool | Prints more verbose information during the validation process     |
| POLL_TIMEOUT      | int  | Time to wait before Selenium starts performing actions            |
| HEADLESS          | bool | Toggles the visibility of the Firefox window                      |
| SAVE_SCREENSHOT   | bool | Toggles if a screenshot is taken of the results page              |
| SCREENSHOT_PATH   | str  | A file path to where the screenshot is saved                      |
| URL               | str  | A URL to the validator (WARNING: changing this can cause crashes) |

## Credits

A special thank you to the following projects:

- [The Python Foundation][5] (for the great programming language and standard library)
- [The Selenium Project][6] (for the web scraping tools)
- [The W3C][7] (for the web standards and online validators)

[0]: https://github.com/netromdk/vermin
[1]: https://pypi.org/project/selenium/
[2]: https://en.wikipedia.org/wiki/Doas
[3]: https://jigsaw.w3.org/css-validator/validator.html.en#validate_by_input
[4]: https://validator.w3.org/nu/#textarea
[5]: https://www.python.org/psf-landing/
[6]: https://www.selenium.dev/
[7]: https://www.w3.org/
