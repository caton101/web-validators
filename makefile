INSTALL_DIR=/usr/local/share
EXEC_DIR=/usr/local/bin

install:
	mkdir -p $(INSTALL_DIR)/web-validators
	cp ./css-validator  $(INSTALL_DIR)/web-validators/css-validator
	cp ./html-validator $(INSTALL_DIR)/web-validators/html-validator
	cp ./LICENSE $(INSTALL_DIR)/web-validators/LICENSE
	ln -s $(INSTALL_DIR)/web-validators/css-validator  $(EXEC_DIR)/css-validator
	ln -s $(INSTALL_DIR)/web-validators/html-validator $(EXEC_DIR)/html-validator

uninstall:
	rm $(EXEC_DIR)/css-validator
	rm $(EXEC_DIR)/html-validator
	rm -rf $(INSTALL_DIR)/web-validators

reinstall:
	make uninstall
	make install
